package ua.khpi.norkin.controller;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.google.gson.JsonObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

/**
 * Created by marknorkin on 24.02.2016.
 */
@RestController
@RequestMapping("/photoservice")
public class PhotoController {

    private static final Logger LOGGER = Logger.getLogger(PhotoController.class);
    @Autowired
    Cloudinary cloudinary;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getFile(@PathVariable("id") String id) {
        String url = cloudinary.url().publicId(id).generate();
        JsonObject response = new JsonObject();
        response.addProperty("url", url);
        return new ResponseEntity<>(response.toString(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> uploadFile(@RequestParam("image") MultipartFile file) {
        try {
            Map data = cloudinary.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
            JsonObject responseData = new JsonObject();
            responseData.addProperty("id", data.get("public_id").toString());
            responseData.addProperty("url", data.get("url").toString());
            return new ResponseEntity<>(responseData.toString(), HttpStatus.CREATED);
        } catch (IOException e) {
            LOGGER.error("Cannot process photo: ", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteFile(@PathVariable("id") String id) {
        try {
            Map data = cloudinary.uploader().destroy(id, ObjectUtils.asMap("invalidate", true));
            HttpStatus status = "ok".equalsIgnoreCase(data.get("result").toString()) ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(status);
        } catch (IOException e) {
            LOGGER.error("Can't delete photo with id: " + id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
