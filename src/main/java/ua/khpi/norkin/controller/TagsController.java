package ua.khpi.norkin.controller;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by marknorkin on 13.03.2016.
 */
@RestController
@RequestMapping("/photoservice/tags")
public class TagsController {
    private static final Logger LOGGER = Logger.getLogger(TagsController.class);

    @Autowired
    Cloudinary cloudinary;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> updateFileTags(@RequestParam("id") String id, @RequestParam("tags") String options) {
        String[] tags = options.split(",");
        try {
            for (String tag : tags) {
                cloudinary.uploader().addTag(tag, new String[]{id}, ObjectUtils.emptyMap());
            }
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IOException e) {
            LOGGER.error("Can't add tags: " + Arrays.toString(tags), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
