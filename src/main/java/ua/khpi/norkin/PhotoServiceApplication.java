package ua.khpi.norkin;

import com.cloudinary.Cloudinary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@SpringBootApplication
public class PhotoServiceApplication {

	@Value("${CLOUDINARY_URL}")
	String cloudinaryEnvVar;

	@Bean
	public Cloudinary cloudinary() {
		return new Cloudinary(cloudinaryEnvVar);
	}

	public static void main(String[] args) {
		SpringApplication.run(PhotoServiceApplication.class, args);
	}
}
